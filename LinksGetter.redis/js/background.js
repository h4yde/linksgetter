// listen for sendMessage() from content script
// 2020 changes: 
//      "chrome.*" instead "browser.*" cuz now is based on chromium.

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        // set the icon for the browser action from sendMessage() in content script
        chrome.browserAction.setIcon({
            path: {
                "48": request.iconPath48,
                "96": request.iconPath96
            },
            tabId: sender.tab.id
        });
        // disable browser action for the current tab
        chrome.browserAction.disable(sender.tab.id);
    });
